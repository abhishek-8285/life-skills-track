# Prevention of Sexual Harassment

## Q.1 What kinds of behaviour cause sexual harassment?

Sexual harassment can encompass a range of behaviors that are unwelcome, unwanted, or offensive of a sexual nature. Some examples include:

1. **Unwanted sexual advances**: Making explicit or implicit sexual propositions or advances without consent.
2. **Verbal or written harassment**: Making derogatory or sexually suggestive comments, jokes, or remarks.
3. **Non-consensual physical contact**: Touching, groping, or other physical contact of a sexual nature without consent.
4. **Displaying offensive material**: Showing or sharing sexually explicit images, videos, or materials in a non-consensual or inappropriate context.
5. **Unwanted sexual attention**: Continuously making unwelcome sexual comments, gestures, or inquiries.
6. **Hostile work or academic environment**: Creating an intimidating, hostile, or offensive atmosphere through sexually explicit conversations, actions, or imagery.
7. **Retaliation for reporting**: Taking adverse actions or retaliating against someone who has reported or spoken up about sexual harassment.

## Q.2 What would you do in case you face or witness any incident or repeated incidents of such behaviour?

If you face or witness any incident or repeated incidents of sexual harassment, here are the steps you can take:

1. **Assess your safety**: Prioritize your safety and well-being. If you feel physically threatened or unsafe, remove yourself from the situation and seek help if needed.
2. **Document the incidents**: Keep a record of the incidents, including dates, times, locations, descriptions, and any witnesses involved. This documentation can be crucial if you decide to report the harassment.
3. **Talk to someone you trust**: Reach out to a supportive colleague, friend, or supervisor to discuss the situation and seek guidance. Sharing your experiences can help you process the incident and determine the best course of action.
4. **Familiarize yourself with policies and procedures**: Understand the policies and procedures in place within your organization or educational institution regarding sexual harassment. This knowledge will guide you on how to report the incidents and what steps may follow.
5. **Report the harassment**: If you feel comfortable doing so, report the incidents to the appropriate authority within your organization or institution, such as a supervisor, human resources department, or designated reporting entity. Provide them with the documented information to support your report.
6. **Seek professional support**: If you are experiencing emotional distress or need additional support, consider reaching out to a counselor, therapist, or a helpline specializing in dealing with sexual harassment.
7. **Be an ally**: If you witness someone else being subjected to sexual harassment, offer support and encourage them to report the incident. Your intervention can make a difference and provide a sense of safety and solidarity.

Remember to follow the specific procedures and policies of your organization or institution when addressing sexual harassment. Each situation may require a unique approach, and seeking guidance from the appropriate channels will help ensure a proper response and resolution.