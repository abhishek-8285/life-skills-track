# Object-Oriented Programming (OOP) Analysis and Code Refactoring Report

## Introduction
This report aims to address the challenges associated with managing a difficult-to-manage codebase in a new project. The goal is to study and implement Object-Oriented Programming (OOP) concepts to refactor the codebase, improving its maintainability and scalability.

## OOP Concepts and Code Samples

### 1. Encapsulation
Encapsulation involves bundling related data and functions into objects, providing data protection and controlled access through methods[^1^]. This enhances code maintainability and reduces dependencies.

**Example:**
```javascript
class Car {
  constructor(brand, model) {
    this.brand = brand;
    this.model = model;
  }

  startEngine() {
    console.log(`car is started`)
  }

  drive() {
    console.log(``)
  }
}

const car = new Car("Toyota", "Camry");
car.startEngine();
car.drive();
```

Reference: [^Mozilla Developer Network - Encapsulation^](https://developer.mozilla.org/en-US/docs/Glossary/Encapsulation)

### 2. Inheritance
Inheritance allows the creation of derived classes based on existing classes, inheriting their properties and behaviors[^2^]. This promotes code reuse and facilitates hierarchical relationships between classes.

**Example:**
```javascript
class Animal {
  constructor(name) {
    this.name = name;
  }

  eat() {
    console.log(`${this.name} is eating`)
  }
}

class Dog extends Animal {
  constructor(name, breed) {
    super(name);
    this.breed = breed;
  }

  bark() {
    console.log(`${this.name} is barking`)
   
  }
}

const dog = new Dog("Buddy", "Labrador");
dog.eat();
dog.bark();
```

Reference: [^MDN Web Docs - Inheritance^](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance)

### 3. Polymorphism
Polymorphism allows objects of different classes to be treated as objects of a common superclass. It provides flexibility in handling different object types through a shared interface, promoting code extensibility and reusability[^3^].

**Example:**
```javascript
class Shape {
  calculateArea() {
    throw new Error("Subclasses must implement calculateArea method");
  }
}

class Rectangle extends Shape {
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
  }

  calculateArea() {
    return this.width * this.height;
  }
}

class Circle extends Shape {
  constructor(radius) {
    super();
    this.radius = radius;
  }

  calculateArea() {
    return 3.14 * this.radius * this.radius;
  }
}

const shapes = [new Rectangle(4, 5), new Circle(3)];
shapes.forEach((shape) => {
  console.log(shape.calculateArea());
});
```

Reference: [^MDN Web Docs - Polymorphism^](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance#polymorphism)

## Conclusion
By incorporating OOP concepts such as encapsulation, inheritance, and polymorphism, we can significantly improve the manageability of the project's codebase. Refactoring the codebase to adhere to these principles enhances code organization, reusability, and maintainability. This leads to better development efficiency and easier code maintenance in the long run.

[^1^]: Mozilla Developer Network. (n.d.). Encapsulation. Retrieved from [here](https://developer.mozilla.org/en-US/docs/Glossary/Encapsulation)

[^2^]: MDN Web Docs. (n.d.). Inheritance. Retrieved from [here](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance)

[^3^]: MDN Web Docs. (n.d.). Polymorphism. Retrieved from [here](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance#polymorphism)